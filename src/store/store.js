// weatherSlice.js
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  city: null,
  currentWeather: null,
  dailyWeather: null,
};

const weatherSlice = createSlice({
  name: 'weather',
  initialState,
  reducers: {
    setCity: (state, action) => {
      state.city = action.payload;
    },
    setCurrentWeather: (state, action) => {
      state.currentWeather = action.payload;
    },
    setDailyWeather: (state, action) => {
      state.dailyWeather = action.payload;
    },
  },
});

export const { setCity, setCurrentWeather, setDailyWeather } = weatherSlice.actions;
export default weatherSlice.reducer;
